# coinmarketcap.com Test Automation



## About

This project is Serenity-BDD test automation framework for extracting data from a Cryptocurrency tracking website.
The Page Object Model design pattern is used in this project where there will be a Java class for each webpage
Once tests have finished executing, a Serenity report will be generated containing the test results and step-by-step detail of what occurred including screenshots.
This will also produce csv files with data that has been extracted from the website.

## Requirements:

* Java 8
* Maven 3
* Either Windows, Mac or Linux
* Chrome version 109 (If you have a different version you can replace the chromedriver as shown in the project directory below)


## How to run:

Test steps are written in feature files that may contain multiple scenarios. You can either run all these files or just certain ones.
Running the automation framework on your local machine is done by opening up a terminal or command line window and navigating ot the directory of the project and entering either of the following command.

##### To run all features:
    mvn clean verify

    
## How to find report

After all tests have been executed, the target folder will be generated and this folder will contain another folder called "serenity-reports".
This folder contains all the components for the report. To open the report you must open the file named "index.html".
The contents of the report will only show if that index.html file is still in a directory with all the other components for the report.

Once the Serenity report is open, to view the test results, click on "Test Results" to see the status of each scenario. To view the results of each test step, click on a scenario. Test steps may contain title cards that can be expanded to reveal extra information.

There is also a single page summary that can be found within the same serenity-reports folder. This file is called "serenity-summary.html".

Console output is also printed.

## Project structure

##### This is a maven project using a conventional Serenity-bdd project structure and Page Object Model design pattern:


    src
        +main
        +test
            +java
                +org.example
                    +pages                                  Directory for page objects
                        +HomePage.java                      Page class for each webpage
                        +Page.java                          Abstract page class with methods that sub classes can inherit
                    +runners
                        +TestSuite.java                     Test runner class where feature path and glue is set
                    +steps
                        +AbstractSteps.java                 Contains methods and variables that step definition classes can inherit including the driver and base url
                        +Hooks.java                         Contains the @Before and @After methods which run for every scenario
                        +StepDefintions.java                Contains the step defintions
                        +StepVariables.java                 Enum class so that Serenity session variables have something be to be mapped to
            +resources
                +features                                   Directory containing all feature files
                    +filter-and-extract-data.feature
                +webdriver                                  Webdriver binaries are bundles here
                    +linux
                        +chromedriver
                    +mac
                        +chromedriver
                    +windows
                        +chromedriver.exe
                +serenity.conf                              Serenity configuration file where configuration such as driver, environment and browser options can be set
    test-data-output                                        This is a directory we CSV files that are generated during data extraction are stored in 
    pom.xml                                                 Dependencies and plugins are located here
    .gitignore                                              This tells git what not to commit such as the target folder
    serenity.properties                                     This files is for configuring the serenity report
               
               
## The serenity.conf file

As stated in the project structure this is a file to configure how the web automation will be done.

By default, it is using Google Chrome.

    webdriver {
        driver = chrome
        user.driver.service.pool = false
    } 
    
By default, it will run the browser in headless mode so it would not open a browser window.
To turn headless mode off, find the line below in the serenity.conf file and set it to false.

    headless.mode = true

Incognito mode has been turned on so that saved cookies, cache and preferences won't interfere with test

    chrome.switches = """--incognito"""
    
Paths to difference webdriver binaries are defined here. They must follow the conventional directory structure.

    drivers {
        windows {
            webdriver.chrome.driver  = "src/test/resources/webdriver/windows/chromedriver.exe"
        }
        linux {
            webdriver.chrome.driver  = "src/test/resources/webdriver/linux/chromedriver"
        }
        mac {
            webdriver.chrome.driver  = "src/test/resources/webdriver/mac/chromedriver"
        }
    }
    
    
Environment properties are also defined. Currently, there is only one environment which is the live environment which has been set as the default environment.
No extra flags are needed to run in the default environment. 
If an extra environment is added, then to run in that environment the flag "-Denvironment={environment name}" is needed in the command for running the tests.

    environments {
        default {
            webdriver.base.url = "https://coinmarketcap.com/"
        }
    }