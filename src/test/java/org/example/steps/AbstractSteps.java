package org.example.steps;

import net.thucydides.core.util.EnvironmentVariables;
import org.openqa.selenium.WebDriver;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;

public class AbstractSteps {
    EnvironmentVariables environmentVariables;
    WebDriver driver = getDriver();
    static String baseUrl;


    public String[][] convertCsvTo2dArray(String file) {
        List<String[]> lines = new ArrayList<>();
        String line ="";
        try{
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            while ((line = bufferedReader.readLine()) !=null){
                lines.add(line.split(","));

            }
        } catch (IOException e){
            e.printStackTrace();
        }
        return lines.toArray(new String[lines.size()][]);
    }


}
