package org.example.steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.core.Serenity;
import org.example.pages.HomePage;
import org.junit.Assert;
import org.openqa.selenium.support.PageFactory;

import java.io.IOException;


public class StepDefinitions extends AbstractSteps{

    HomePage homePage = PageFactory.initElements(driver, HomePage.class);


    @Given("user is on the coin market cap website")
    public void userIsOnTheCoinMarketCapWebsite() {
        driver.manage().window().maximize();
        driver.get(baseUrl);
        System.out.println("base url is "+baseUrl);
        System.out.println("webdriver is is "+ driver);
        Assert.assertEquals(homePage.getPath(), driver.getCurrentUrl());
    }

    @When("user changes number of rows to {string}")
    public void userChangesNumberOfRowsTo(String size) {
        homePage.selectPageSize(size);
    }

    @And("close popup if appears")
    public void closePopupIfAppears() {
        homePage.closePopup();
        homePage.closeCookieBanner();
        homePage.closeSurveyPopup();

    }

    @Then("cmc table is now showing {int} rows per page")
    public void cmcTableIsNowShowingRowsPerPage(int rows) {
        Assert.assertEquals(rows, homePage.getRowCount());
    }


    @And("table results is displaying coin market cap data and is saved to file {string}")
    public void tableResultsIsDisplayingCoinMarketCapDataAndIsSavedToFile(String fileName) throws IOException, InterruptedException {
        Thread.sleep(1000);
        homePage.getTableData(fileName);
        try {
            Serenity.recordReportData().withTitle("Unfiltered table displaying coin data").andContents(Serenity.sessionVariableCalled(StepVariables.COIN_DATA_TABLE));

        }catch (NullPointerException e){
            System.out.println("Skip recording report data");
        }
    }

    @When("user filters by Algorithm {string}")
    public void userFiltersByAlgorithm(String algorithm) throws InterruptedException {
        homePage.clickFiltersButton();
        homePage.selectAlgorithmFilter(algorithm);

    }

    @And("user clicks on plus filter")
    public void userClicksOnPlusFilter() {
        homePage.clickPlusFilterButton();
    }

    @And("user clicks mineable toggle")
    public void userClicksMineableToggle() {
        homePage.clickMineableToggle();
    }


    @And("user select {string} in all cryptocurrencies filter")
    public void userSelectInAllCryptocurrenciesFilter(String filter) {
        homePage.selectAllCryptocurrencies(filter);
    }

    @And("user selects price and set minimum value as {string} and maximum as {string}")
    public void userSelectsPriceAndSetMinimumValueAsAndMaximumAs(String min, String max) {
        homePage.setMinimumAndMaximumPrice(min, max);
    }

    @And("user clicks the apply filters button")
    public void userClicksTheApplyFiltersButton() throws InterruptedException {
        homePage.clickApplyFiltersButton();
        homePage.clickShowResultsButton();
    }

    @Then("filters are applied")
    public void filtersAreApplied() {
        Assert.assertEquals("PoW", homePage.getAlgorithmFilter());
        Assert.assertEquals("+ 3 More Filters", homePage.getNumberOfMoreFiltersApplied());
    }


    @And("compare content from {string} to content from {string}")
    public void compareContentFromToContentFrom(String file1, String file2) {
        String folder = "test-data-output/";
        System.out.println("Highest position coin in unfiltered table is "+ convertCsvTo2dArray(folder+file1)[1][1] + " with price of " +convertCsvTo2dArray(folder+file1)[1][2] + " and market cap of " + convertCsvTo2dArray(folder+file1)[1][7]);
        System.out.println("Highest position coin in filtered table is "+ convertCsvTo2dArray(folder+file2)[1][1] + " with price of " +convertCsvTo2dArray(folder+file2)[1][2] + " and market cap of " + convertCsvTo2dArray(folder+file2)[1][7]);
        try {
            Serenity.recordReportData().withTitle("Highest position coin from each table").andContents("Highest position coin in unfiltered table is "+ convertCsvTo2dArray(folder+file1)[1][1] + " with price of " +convertCsvTo2dArray(folder+file1)[1][2] + " and market cap of " + convertCsvTo2dArray(folder+file1)[1][7] +"\n" + "Highest position coin in filtered table is "+ convertCsvTo2dArray(folder+file2)[1][1] + " with price of " +convertCsvTo2dArray(folder+file2)[1][2] + " and market cap of " + convertCsvTo2dArray(folder+file2)[1][7]);

        }catch (NullPointerException e){
            System.out.println("Skip recording report data");
        }
    }
}
