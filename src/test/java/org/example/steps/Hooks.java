package org.example.steps;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;

import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.useDriver;

public class Hooks extends AbstractSteps{
    @Before
    public void init(){
        useDriver(driver);
        baseUrl = EnvironmentSpecificConfiguration.from(environmentVariables).getProperty("webdriver.base.url");
        System.out.println(driver);

    }

    @After
    public void tearDown(){
        driver.quit();
    }
}
