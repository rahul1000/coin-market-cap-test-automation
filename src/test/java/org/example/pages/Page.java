package org.example.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;

public abstract class Page {

    public WebDriver driver = getDriver();
    Actions action = new Actions(driver);


    public void waitForElementVisible(WebElement element, int timeout){
        WebDriverWait webDriverWait = new WebDriverWait(driver, Duration.ofSeconds(timeout));
        webDriverWait.until(ExpectedConditions.visibilityOf(element));
    }

    public void waitForElementClickable(WebElement element, int timeout){
        WebDriverWait webDriverWait = new WebDriverWait(driver, Duration.ofSeconds(timeout));
        webDriverWait.until(ExpectedConditions.elementToBeClickable(element));
    }
}
