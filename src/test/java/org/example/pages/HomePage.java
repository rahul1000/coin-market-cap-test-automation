package org.example.pages;

import com.opencsv.CSVWriter;
import net.serenitybdd.core.Serenity;
import org.example.steps.StepVariables;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.io.*;
import java.util.Formatter;
import java.util.List;

public class HomePage extends Page{
    String path = "https://coinmarketcap.com/";

    public String getPath(){
        return path;
    }


    @FindBy(css ="div[class $= 'table-link-area']") private WebElement tableLinkArea;
    @FindBy(css ="div[class $= 'table-control-area']") private WebElement tableControlArea;
    @FindBy(css ="div[class $= 'table-control-page-sizer']") private WebElement tableControlPageSizer;
    @FindBy(css ="div[class = 'tippy-content'][data-state = 'visible']") private WebElement visibleTippyContent;
    @FindBy(css ="div[class $= 'popup__contents']") private WebElement popupContent;
    @FindBy(css ="svg[class $= 'close-button']") private WebElement popupCloseButton;
    @FindBy(id ="cmc-cookie-policy-banner") private WebElement cookieBanner;
    @FindBy(css ="table[class $= 'cmc-table  ']") private WebElement cmcTable;
    @FindBy(css ="button[class $= 'table-control-filter']") private WebElement filtersButton;



    public void selectPageSize(String size){
        action.moveToElement(tableControlArea).perform();
        tableControlPageSizer.findElement(By.cssSelector("div[data-sensors-click = 'true']")).click();
        List<WebElement> pageSizes = visibleTippyContent.findElements(By.tagName("button"));
        boolean buttonNotFound = true;
        int i = 0;
        do {
            if(pageSizes.get(i).getText().equals(size)){
                buttonNotFound = false;
            }
            i++;
        }
        while (buttonNotFound);
        pageSizes.get(i-1).click();
    }

    public void closePopup(){
        try{
            waitForElementVisible(popupContent,5 );
            waitForElementClickable(popupCloseButton, 5);
            popupCloseButton.click();
        }catch (Exception e){
            System.out.println("No Popup");
        }
    }

    public void closeCookieBanner(){
        try{
            waitForElementVisible(cookieBanner,5 );
            waitForElementClickable(cookieBanner.findElement(By.className("cmc-cookie-policy-banner__close")), 5);
            cookieBanner.findElement(By.className("cmc-cookie-policy-banner__close")).click();
        }catch (Exception e){
            System.out.println("No Popup");
        }
    }

    public void closeSurveyPopup(){
        WebElement surveyThumbnail = driver.findElement(By.cssSelector("img[src='https://s2.coinmarketcap.com/static/cloud/img/survey_entrance_thumb.png']"));
        try{
            waitForElementVisible(surveyThumbnail,5 );
            waitForElementClickable(surveyThumbnail, 5);

            WebElement surveyPopup = surveyThumbnail.findElement(By.xpath("./.."));
            WebElement surveyPopupButtons = surveyPopup.findElement(By.className("buttons"));
            surveyPopupButtons.findElements(By.tagName("span")).get(1).click();
        }catch (Exception e){
            System.out.println("No Popup");
        }
    }

    public int getRowCount(){
        waitForElementClickable(cmcTable,5);
        return cmcTable.findElement(By.tagName("tbody")).findElements(By.tagName("tr")).size();
    }

    public WebElement getCmcTableRow(int row){
        return cmcTable.findElement(By.tagName("tbody")).findElements(By.tagName("tr")).get(row);
    }

    public List<WebElement> getCmcTableCell(int row){
        return getCmcTableRow(row).findElements(By.tagName("td"));
    }

    public String getRankFromTable(int row){
        return getCmcTableCell(row).get(1).getText();
    }

    public String getNameFromTable(int row){
        return getCmcTableCell(row).get(2).findElements(By.tagName("p")).get(0).getText();
    }

    public String getPriceFromTable(int row){
        return getCmcTableCell(row).get(3).getText().replace(",","");
    }

    public String get24hVolumeFromTable(int row){
        return getCmcTableCell(row).get(8).findElements(By.tagName("p")).get(0).getText().replace(",","") + " / "+ getCmcTableCell(row).get(8).findElements(By.tagName("p")).get(1).getText().replace(",","");
    }

    public String get1hPercentFromTable(int row){
        String plusOrMinus = "";
        WebElement cellContents = getCmcTableCell(row).get(4).findElement(By.tagName("span"));
        if (cellContents.findElement(By.tagName("span")).getAttribute("class").equals("icon-Caret-down")){
            plusOrMinus = "-";
        }
        if (cellContents.findElement(By.tagName("span")).getAttribute("class").equals("icon-Caret-up")){
            plusOrMinus = "+";
        }
        return plusOrMinus + getCmcTableCell(row).get(4).getText().replace(",","");
    }

    public String get24hPercentFromTable(int row){
        String plusOrMinus = "";
        WebElement cellContents = getCmcTableCell(row).get(5).findElement(By.tagName("span"));
        if (cellContents.findElement(By.tagName("span")).getAttribute("class").equals("icon-Caret-down")){
            plusOrMinus = "-";
        }
        if (cellContents.findElement(By.tagName("span")).getAttribute("class").equals("icon-Caret-up")){
            plusOrMinus = "+";
        }
        return plusOrMinus + getCmcTableCell(row).get(5).getText().replace(",","");
    }

    public String get7dPercentFromTable(int row){
        String plusOrMinus = "";
        WebElement cellContents = getCmcTableCell(row).get(6).findElement(By.tagName("span"));
        if (cellContents.findElement(By.tagName("span")).getAttribute("class").equals("icon-Caret-down")){
            plusOrMinus = "-";
        }
        if (cellContents.findElement(By.tagName("span")).getAttribute("class").equals("icon-Caret-up")){
            plusOrMinus = "+";
        }
        return plusOrMinus + getCmcTableCell(row).get(6).getText().replace(",","");
    }

    public String getMarketCapFromTable(int row){
        return getCmcTableCell(row).get(7).getText().replace(",","");
    }

    public String getCirculatingSupplyFromTable(int row){
        return getCmcTableCell(row).get(9).getText().replace(",","");
    }


    public void getTableData(String fileName) throws IOException {
        CSVWriter write = new CSVWriter(new FileWriter("test-data-output/"+fileName));
        String[] tableHeading = {"Rank", "Name", "Price",  "1h%", "24h%", "7d%", "Market Cap", "Volume(24h)", "Circulating Supply"};
        write.writeNext(tableHeading);
        System.out.println();
        System.out.println(fileName);
        System.out.printf("-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------%n");
        System.out.printf("| %-5s | %-18s | %20s | %8s | %8s | %8s | %20s | %40s | %30s |%n","Rank", "Name", "Price",  "1h%", "24h%", "7d%", "Market Cap", "Volume(24h)", "Circulating Supply");
        System.out.printf("-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------%n");
        Formatter formatter = new Formatter();

        formatter.format("| %-5s | %-18s | %20s | %8s | %8s | %8s | %20s | %40s | %30s |%n","Rank", "Name", "Price",  "1h%", "24h%", "7d%", "Market Cap", "Volume(24h)", "Circulating Supply");
        for (int i = 0; i<getRowCount(); i++){
            action.moveToElement(getCmcTableRow(i)).perform();
            String[] coinData = {getRankFromTable(i),getNameFromTable(i), getPriceFromTable(i), get1hPercentFromTable(i), get24hPercentFromTable(i), get7dPercentFromTable(i), getMarketCapFromTable(i), get24hVolumeFromTable(i), getCirculatingSupplyFromTable(i)};
            write.writeNext(coinData);
            System.out.printf("| %-5s | %-18s | %20s | %8s | %8s | %8s | %20s | %40s | %30s |%n",getRankFromTable(i),getNameFromTable(i), getPriceFromTable(i), get1hPercentFromTable(i), get24hPercentFromTable(i), get7dPercentFromTable(i), getMarketCapFromTable(i), get24hVolumeFromTable(i),  getCirculatingSupplyFromTable(i));
            formatter.format("| %-5s | %-18s | %20s | %8s | %8s | %8s | %20s | %40s | %30s |%n",getRankFromTable(i),getNameFromTable(i), getPriceFromTable(i), get1hPercentFromTable(i), get24hPercentFromTable(i), get7dPercentFromTable(i), getMarketCapFromTable(i), get24hVolumeFromTable(i),  getCirculatingSupplyFromTable(i));
        }
        System.out.printf("-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------%n");

        Serenity.setSessionVariable(StepVariables.COIN_DATA_TABLE).to(formatter.toString());
        write.flush();
        System.out.println("Data entered");
    }


    public void clickFiltersButton() {
        action.moveToElement(tableControlArea).perform();

        List<WebElement> buttons = driver.findElements(By.cssSelector("button[class $= 'table-control-filter']"));
        boolean buttonNotFound = true;
        int i = 0;
        do {
            if(buttons.get(i).getText().contains("Filters")){
                buttonNotFound = false;
            }
            i++;
        }
        while (buttonNotFound);
        buttons.get(i-1).click();
    }

    public void selectAlgorithmFilter(String algorithm) throws InterruptedException {

        List<WebElement> filters = driver.findElements(By.cssSelector("li[class = 'filter']"));
        System.out.println(filters.size());
        filters.get(1).findElement(By.tagName("button")).click();
        action.sendKeys(algorithm).build().perform();

        List<WebElement> algorithmOptions = driver.findElements(By.cssSelector("li[class='optionGroupItem']"));
        boolean buttonNotFound = true;
        int i = 0;
        do {
            if(algorithmOptions.get(i).getText().equals(algorithm)){
                buttonNotFound = false;
            }
            i++;
        }
        while (buttonNotFound);
        algorithmOptions.get(i-1).click();
        Thread.sleep(2000);
    }

    public void clickPlusFilterButton(){
        List<WebElement> buttons = driver.findElements(By.tagName("li"));
        boolean buttonNotFound = true;
        int i = 0;
        do {
            if(buttons.get(i).getText().contains("Add Filter")){
                buttonNotFound = false;
            }
            i++;
        }
        while (buttonNotFound);
        buttons.get(i-1).click();
    }

    public void clickMineableToggle(){
        List<WebElement> moreFiltersOptions = driver.findElements(By.cssSelector("button[data-qa-id='filter-dd-toggle']"));
        boolean buttonNotFound = true;
        int i = 0;
        do {
            if(moreFiltersOptions.get(i).getText().equals("Mineable")){
                buttonNotFound = false;
            }
            i++;
        }
        while (buttonNotFound);
        moreFiltersOptions.get(i-1).findElement(By.cssSelector("label[id = 'mineable']")).click();
    }

    public void selectAllCryptocurrencies(String filter){
        List<WebElement> moreFiltersOptions = driver.findElements(By.cssSelector("button[data-qa-id='filter-dd-toggle']"));
        boolean buttonNotFound = true;
        int i = 0;
        do {
            if(moreFiltersOptions.get(i).getText().equals("All Cryptocurrencies")){
                buttonNotFound = false;
            }
            i++;
        }
        while (buttonNotFound);
        moreFiltersOptions.get(i-1).click();

        List<WebElement> cryptoOptions = driver.findElements(By.cssSelector("button[class $= 'cmc-option-button'][data-sensors-click = 'true']"));
        boolean optionNotFound = true;
        int j = 0;
        do {
            if(cryptoOptions.get(j).getText().equals(filter)){
                optionNotFound = false;
            }
            j++;
        }
        while (optionNotFound);
        cryptoOptions.get(j-1).click();


    }

    public void setMinimumAndMaximumPrice(String min, String max){
        List<WebElement> moreFiltersOptions = driver.findElements(By.cssSelector("button[data-qa-id='filter-dd-toggle']"));
        boolean buttonNotFound = true;
        int i = 0;
        do {
            if(moreFiltersOptions.get(i).getText().equals("Price")){
                buttonNotFound = false;
            }
            i++;
        }
        while (buttonNotFound);
        moreFiltersOptions.get(i-1).click();

        driver.findElement(By.cssSelector("input[data-qa-id='range-filter-input-min']")).sendKeys(min);
        driver.findElement(By.cssSelector("input[data-qa-id='range-filter-input-max']")).sendKeys(max);

    }

    public void clickApplyFiltersButton() throws InterruptedException {
        List<WebElement> buttons = driver.findElements(By.cssSelector("button[data-qa-id='filter-dd-button-apply']"));
        boolean buttonNotFound = true;
        int i = 0;
        do {
            if(buttons.get(i).getText().equals("Apply Filter")){
                buttonNotFound = false;
            }
            i++;
        }
        while (buttonNotFound);
        buttons.get(i-1).click();
        Thread.sleep(2000);


    }

    public void clickShowResultsButton() throws InterruptedException {

        List<WebElement> buttons = driver.findElements(By.cssSelector("button[class $= 'cmc-filter-button']"));
        boolean buttonNotFound = true;
        int i = 0;
        do {
            if(buttons.get(i).getText().equals("Show results")){
                buttonNotFound = false;
            }
            i++;
        }
        while (buttonNotFound);
        buttons.get(i-1).click();
        Thread.sleep(2000);
    }

    public String getAlgorithmFilter(){
        List<WebElement> filters = driver.findElements(By.cssSelector("li[class = 'filter']"));
        return filters.get(1).findElement(By.tagName("button")).getText();
    }

    public String getNumberOfMoreFiltersApplied(){
        List<WebElement> buttons = driver.findElements(By.tagName("li"));
        boolean buttonNotFound = true;
        int i = 0;
        do {
            if(buttons.get(i).getText().contains("More Filters")){
                buttonNotFound = false;
            }
            i++;
        }
        while (buttonNotFound);

        return buttons.get(i-1).findElement(By.tagName("button")).getText();
    }

}
