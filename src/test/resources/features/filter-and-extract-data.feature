Feature: Filtering and extracting data
  Scenario: Filtering UI and extracting data for comparison
    Given user is on the coin market cap website
    And close popup if appears
    When user changes number of rows to "20"
    Then cmc table is now showing 20 rows per page
    And table results is displaying coin market cap data and is saved to file "UnfilteredCoinDataTable.csv"
    When user filters by Algorithm "PoW"
    And user clicks on plus filter
    And user clicks mineable toggle
    And user select "Coins" in all cryptocurrencies filter
    And user selects price and set minimum value as "100" and maximum as "10,000"
    And user clicks the apply filters button
    Then filters are applied
    And table results is displaying coin market cap data and is saved to file "FilteredCoinDataTable.csv"
    And compare content from "UnfilteredCoinDataTable.csv" to content from "FilteredCoinDataTable.csv"
